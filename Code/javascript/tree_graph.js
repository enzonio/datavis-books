/* Consider the data */
/* var data = {
    'nodes':[
        { 'id': 'Machin' },
        { 'id': 'Chouette' },
        { 'id': 'Chose' },
        { 'id': 'Rien' },
    ],

    'links':[
        { 'source': 'Machin', 'target':'Chouette', 'value':1 },
        { 'source': 'Machin', 'target':'Rien', 'value':1 },
        { 'source': 'Machin', 'target':'Chose', 'value':1 },
        { 'source': 'Chose', 'target':'Rien', 'value':1 },
    ]
} */

/* Adapt the size of the chart to the window */
var width = parseInt(window.innerWidth, 10) - 50;
var height = parseInt(window.innerHeight, 10) - 50;
var svg = document.getElementById('booksGraph');

function adaptSize()
{
    svg.setAttribute('width', width);
    svg.setAttribute('height', height);
}

adaptSize();

/* GRAPH */

// Drag and drop
var drag_handler = d3.drag().on("drag", drag_drag)
    .on('start', drag_start)
    .on('end', drag_end);

function drag_start(d)
{
    if (!d3.event.active) simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
}

function drag_drag(d)
{
    d.fx = d3.event.x ;
    d.fy = d3.event.y ;
}

function drag_end(d)
{
    if (!d3.event.active) simulation.alphaTarget(0);
    d.fx = null ;
    d.fy = null;
}
// End drag and drop

var booksGraph = d3.select('#booksGraph').append('g').attr('class', 'container');

// Set up the simulation
var simulation = d3.forceSimulation().nodes(data.nodes);
simulation.force('charge_force', d3.forceManyBody())
    .force('center_force', d3.forceCenter(width/2, height/2));

// Add the links
var links = booksGraph.append('g').attr('class', 'links')
    .selectAll('line')
    .data(data.links).enter()
    .append('line')
    .attr('stroke-width', 1)
    .attr('stroke', 'grey');

// Add the circles
var circles = booksGraph.append('g').selectAll('circle').data(data.nodes).enter().append('circle')
    .attr('fill', function(d){ return d3.interpolateRainbow(d.group / 4); })
    .attr('r', 5)
    .on('click', function(d){ return show_window(d.id);})
    .on('mouseover', function(d){return change_cover(d)});

// Update the location of every nodes at each tick
function tickActions()
{
    circles.attr('cx', function(d){ return d.x; })
        .attr('cy', function(d){ return d.y; })

    links.attr('x1', function(d){ return d.source.x ;})
        .attr('y1', function(d){ return d.source.y ;})
        .attr('x2', function(d){ return d.target.x ; })
        .attr('y2', function(d){ return d.target.y ; })
}

simulation.on('tick', tickActions);

// Setting up the forces
var link_force = d3.forceLink(data.links).id(function(d){return d.id ;}).distance(function(d){return (1/d.value)*50; });
simulation.force('links', link_force);

// Drag and drop
drag_handler(circles);


// Add zooming
var zoom_handler = d3.zoom().on("zoom", zoom_actions);
svg = d3.select('svg');
zoom_handler(svg);
function zoom_actions(){
    booksGraph.attr("transform", d3.event.transform)
}