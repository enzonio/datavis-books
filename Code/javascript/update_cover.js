var coverWindow = document.getElementById('coverWindow');
var coverBook = document.getElementById('coverBook');
var infoBook = document.getElementById('infoBook');

function change_cover(d)
{
    // coverBook.innerHTML = '<img src="' + d["Image-URL"] + '" />';
    document.getElementById("book-title").innerHTML = d.Title;
    document.getElementById("book-author").innerHTML = d.Author;
    document.getElementById("book-year").innerHTML = d.Year;
    document.getElementById("book-publisher").innerHTML = d.Publisher;

}