import csv, json
import time,sys,datetime

removed_file = "sanitization/removed.txt"
path_books = "datasets/Books.csv"
path_users = "datasets/Users.csv"
path_ratings = "datasets/Ratings.csv"

def get_removed_books(filename):
    removed_books = []
    with open(filename, 'r') as f:
        for line in f:
            r1,r2 = line[:-1].split(";")
            removed_books.append((r1,r2))
    return removed_books

def count_lines(fname):
    with open(fname,encoding='iso-8859-15') as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def remaining_time(i, total_i, step_decription):
    t_loop = time.time()
    ellapsed_time = t_loop - t0
    time_remaining = int((ellapsed_time/i)*(total_i-i))
    str_time_remaining = str(datetime.timedelta(seconds=time_remaining))
    str_time_ellapsed = str(datetime.timedelta(seconds=int(ellapsed_time)))
    sys.stdout.write('\r')
    sys.stdout.write(step_decription + " {0:.2f}%, {1}/{2} estimated remaining time : {3} (total time {4})".format(100*i/total_i, i ,total_i, str_time_remaining,str_time_ellapsed))
    sys.stdout.flush()



def remove_from_ratings(removed_books):
    with open(path_ratings, encoding='utf-8') as ratings_csv:
        csv_reader = csv.DictReader(ratings_csv, delimiter=";")
        kept = []
        removed_users = []
        removed_books_isbns = [b[0] for b in removed_books]
        l = 0
        for row in csv_reader:
            d = dict(row)
            found = False
            for i in range(len(removed_books_isbns)):
                if removed_books_isbns[i] == d["ISBN"]:
                    found = True
                    if removed_books[i][1] == "REMOVED":
                        # book has been removed
                        removed_users.append(d["User-ID"])
                    else:
                        # book has been removed because there is another with same title
                        d["ISBN"] = removed_books[i][1]
                        kept.append(d)
                    break
            if found == False:
                kept.append(d)
            l+=1
            if l%100==0:
                remaining_time(l,total_ratings,"Sanitizing ratings")
    with open('sanitization/ratings.json', 'w') as fout:
        json.dump(kept, fout, indent=2)
    print("\n{0} Ratings, {1} removed ({2:.2f}%)".format(total_ratings, len(removed_users), (len(removed_users)/total_ratings)*100))
    return (removed_users)
            

def sanitize_users(removed_users):
    total_i = count_lines(path_users)
    with open("sanitization/ratings.json") as json_file:
        list_reviews = json.load(json_file)
    users_to_remove = []
    for i,u in enumerate(removed_users):
        found = False
        for r in list_reviews:
            if r["User-ID"] == u:
                found=True
                break
        if found==False:
            users_to_remove.append(u)
        if i==1 or i%10==0:
            remaining_time(i+1,len(removed_users),"Finding users to remove")
    
    users_kept = []
    with open(path_users, encoding='iso-8859-15') as users_csv:
        csv_reader = csv.DictReader(users_csv, delimiter=";")
        for row in csv_reader:
            d = dict(row)
            if d["User-ID"] not in users_to_remove:
                users_kept.append(d)
    with open('sanitization/users.json', 'w') as fout:
        json.dump(users_kept, fout, indent=2)
    print("\n{0} Users, {1} removed ({2:.2f}%)".format(total_users, len(users_to_remove), (len(users_to_remove)/total_users)*100))




if __name__ == "__main__":
    print("[*] Count ratings")
    total_ratings = count_lines(path_ratings)
    print("[*] Count users")
    total_users = count_lines(path_users)
    print("[*] Get removed books")
    removed_books = get_removed_books(removed_file)
    t0 = time.time()
    print("[*] Remove ratings")
    removed_users = remove_from_ratings(removed_books)
    t0 = time.time()
    print("[*] Remove users")
    sanitize_users(removed_users)