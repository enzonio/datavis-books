import csv
import re
import json
import requests
import sys,time, datetime

path_books = "datasets/Books.csv"
# path_books = "datasets/10k.csv"
path_users = "datasets/Users.csv"
path_ratings = "datasets/Ratings.csv"

YEAR_MIN = 1000
# the crawl of the database was done in 2004
YEAR_MAX = 2004





def remaining_time(i, total_i, step_decription):
    t_loop = time.time()
    ellapsed_time = t_loop - t0
    time_remaining = int((ellapsed_time/i)*(total_i-i))
    str_time_remaining = str(datetime.timedelta(seconds=time_remaining))
    str_time_ellapsed = str(datetime.timedelta(seconds=int(ellapsed_time)))
    sys.stdout.write('\r')
    sys.stdout.write(step_decription + " {0:.2f}%, {1}/{2} estimated remaining time : {3} (total time {4})".format(100*i/total_i, i ,total_i, str_time_remaining,str_time_ellapsed))
    sys.stdout.flush()


def capital_letters(s):
    return re.sub("(^|\s)(\S)", lambda m: m.group(1) + m.group(2).upper(), s)

def count_lines(fname):
    with open(fname,encoding='iso-8859-15') as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def check_year(d,year_min, year_max, replace = 1000):
    try:
        x = re.search('[0-9]{4}', d['Year'])
    except:
        print(d)
        return 0
    if x is not None:
        year = int(x.group(0))
        if (year >= year_min and year <= year_max):
            return True
        else:
            return False
    else:
        return False

def sanitize_books(path_books):
    kept_books = []
    removed_books = []
    i = 0
    total_lines = count_lines(path_books)
    removed_reason = {"year":0, "title":0, "author":0}
    with open(path_books, encoding='utf-8') as books_csv:
        csv_reader = csv.DictReader(books_csv, delimiter=";")
        for row in csv_reader:
            try:
                d = dict(row)
                keep = True
                # if year is not acceptable replace it by 0
                if check_year(d, YEAR_MIN, YEAR_MAX, replace=0) == False:
                    removed_reason["year"] += 1
                    removed_books.append((d["ISBN"], "REMOVED"))
                    keep = False
                else:
                    # book has valid year, check if title already exists
                    for b in kept_books:
                        b_author_last_name = b["Author"].split(' ')[-1].lower()
                        d_author_last_name = d["Author"].split(' ')[-1].lower()
                        if b["Title"].lower() == d["Title"].lower() and b_author_last_name == d_author_last_name:
                            # print("{}, {} is the same as {}, {}".format(d["Book-Title"], d["Book-Author"],b["Book-Title"], b["Book-Author"]))
                            removed_reason["title"] += 1
                            removed_books.append((d["ISBN"], b["ISBN"]))
                            keep = False
                            break

                if keep:
                    d["Author"] = d["Author"].title()
                    if d["Title"].isupper(): 
                        d["Title"] = capital_letters(d["Title"])
                    kept_books.append(d)
            except Exception as e:
                print("ERREUR : ", e, " , row:", d)
            i += 1
            if i%100 == 0:
                remaining_time(i,total_lines,"Cleaning book database")
    kept = len(kept_books)
    rm = len(removed_books)
    summary = "{0} Books\n-{1} books removed because of incorrect year (between {2} and {3}, dataset was crawled in 2004)\n-{4} books removed because same title and author already existed\n\n{5} books kept ({6:.2f}%)".format(kept+rm, removed_reason["year"], YEAR_MIN, YEAR_MAX, removed_reason["title"], kept, 100*kept/(rm+kept))
    print("\n\n",summary)
    with open("sanitization/output.txt",'w') as f:
        f.write(summary)
    # keep kept books in a file
    with open('sanitization/books.json', 'w') as fout:
        json.dump(kept_books, fout, indent=2)
    # keep removed books ids in a file
    with open('sanitization/removed.txt', 'w') as f:
        for i in removed_books:
            f.write(i[0] + ';' + i[1] +"\n")
    return(kept_books, removed_books)


if __name__ == "__main__":
    t0 = time.time()
    kept_books, removed_books = sanitize_books(path_books)
