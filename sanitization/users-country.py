import json
USERS = "sanitization/users_location.json"

regions_dict = {'usa':"North America", 'canada':"North America", 'united kingdom':"Europe", 'germany':"Europe", 'spain':"Europe", 'australia':"Oceania", 'italy':"Europe", 'france':"Europe", 'portugal':"Europe", 'new zealand':"Oceania", 'netherlands':"Europe", 'switzerland':"Europe", 'brazil':"South America", 'china':"Asia", 'sweden':"Europe", 'india':"Asia", 'austria':"Europe", 'malaysia':"Asia", 'argentina':"South America", 'singapore':"Asia", 'finland':"Europe", 'mexico':"Asia", 'belgium':"Europe", 'denmark':"Europe", 'ireland':"Europe", 'philippines':"Asia", 'turkey':"Asia", 'poland':"Europe", 'pakistan':"Asia", 'greece':"Europe", 'iran':"Asia", 'romania':"Europe", 'chile':"South America", 'israel':"Asia", 'south africa':"Africa", 'indonesia':"Asia", 'norway':"Europe", 'japan':"Asia", 'croatia':"Europe", 'nigeria':"Africa", 'south korea':"Asia", 'slovakia':"Europe", 'czech republic':"Europe", 'yugoslavia':"Europe", 'russia':"Asia", 'hong kong':"Asia", 'taiwan':"Asia", 'costa rica':"South America", 'slovenia':"Europe", 'peru':"South America"}

def countries_dict(top=50):
    with open(USERS) as json_file:
        list_users = json.load(json_file)
        countries = {}
        for u in list_users:
            country = u["Location"].split(",")[-1][1:]
            if country != "": 
                if country not in countries:
                    countries[country] = 1
                else:
                    countries[country] += 1
        sorted_countries = sorted(countries.items(), key=lambda kv: kv[1], reverse=True)[:top]
        top_countries = [t[0] for t in sorted_countries]
        users = []
        for u in list_users:
            country = u["Location"].split(",")[-1][1:]
            if country not in top_countries:
                country = "other"
                region = "other"
            else:
                region = regions_dict[country]
                if country == 'usa':
                    country = 'USA'
                else:
                    country = country.capitalize()
            u_clean = {"User_ID": u["User-ID"], "Age":u["Age"], "Country":country, "Region":region}
            users.append(u_clean)
        with open('sanitization/users.json', 'w') as fout:
            json.dump(users, fout, indent=2)




if __name__ == "__main__":
    countries_dict()