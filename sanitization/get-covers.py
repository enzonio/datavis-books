import json, csv
import time,datetime,sys

BOOKS = "datasets/jsons/books.json"
BOOKS_csv = "datasets/BX-Book/BX-Books.csv"

def remaining_time(i, total_i, step_decription=""):
    t_loop = time.time()
    ellapsed_time = t_loop - t0
    time_remaining = int((ellapsed_time/i)*(total_i-i))
    str_time_remaining = str(datetime.timedelta(seconds=time_remaining))
    str_time_ellapsed = str(datetime.timedelta(seconds=int(ellapsed_time)))
    sys.stdout.write('\r')
    sys.stdout.write(step_decription + " {0:.2f}%, {1}/{2} estimated remaining time : {3} (total time {4})".format(100*i/total_i, i ,total_i, str_time_remaining,str_time_ellapsed))
    sys.stdout.flush()

def add_covers():
    with open(BOOKS, encoding="utf-8") as json_file:
        list_books = json.load(json_file)
        export_list_books = []
        list_csv = []
        n = len(list_books)
        with open(BOOKS_csv,encoding='latin-1') as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=";")
            for row in csv_reader:
                b = dict(row)
                list_csv.append(b)
        for i,b in enumerate(list_books):
            if (i+1)%200 == 0:
                remaining_time(i+1,n)
            for b_csv in list_csv:
                if b_csv["ISBN"] == b["ISBN"]:
                    b["Image-URL"] = b_csv["Image-URL-L"]
                    b["Title"] = b_csv["Book-Title"]
                    b["Author"] = b_csv["Book-Author"]
                    b["Publisher"] = b_csv["Publisher"]
                    export_list_books.append(b)
                    break

        print("\n\nJSON {}, CSV {}, export {}".format(len(list_books), len(list_csv), len(export_list_books)))
        with open('sanitization/books_url.json', 'w') as fout:
            json.dump(export_list_books, fout, indent=2, ensure_ascii=False)

t0 = time.time()
add_covers()