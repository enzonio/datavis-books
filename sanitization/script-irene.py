import csv
import re
import json


def read_books(path):
    liste_books = []
    with open(path, encoding='iso-8859-15') as csvfile:
        reader = list(csv.reader(csvfile, delimiter=';'))
        header_dict = reader[0]

        for row in reader[1:]:
            dict = {}

            for index, header in enumerate(header_dict):
                if header != '':
                    dict[header] = row[index]

            x = re.search('[0-9]{4}', sanitize_line(dict)['Year-Of-Publication'])
            if x is not None:
                liste_books.append(sanitize_line(dict))

    with open('data/books.json', 'w') as fout:
        json.dump(liste_books, fout)
    print(liste_books)
    return liste_books


def sanitize_line(dict):
    """Correct the dataset and return a good line"""
    sanitized = dict

    # Check if publication year is not 0
    if sanitized['Year-Of-Publication'] == '0':
        sanitized['Year-Of-Publication'] = '1950'  # Choose an arbitrary date

    return sanitized


def read_json(path):
    with open(path, "r") as read_file:
        data = json.load(read_file)
        return data


def liste_auteurs(data):
    """Returns the list of different authors"""
    auteurs = []
    for line in data:
        auteur = line['Book-Author']
        auteur = re.sub('"', '', auteur)
        auteur = re.sub('&amp', '\'', auteur)
        if auteur not in auteurs:
            auteurs.append(auteur)
    print(auteurs)
    return auteurs


# ISBN, Book-Title, Book-Author, Year-Of-Publication, Publisher, Image-URL-S, Image-URL-M, Image-URL-L
# read_books('data/BX-Books.csv') CREATE THE JSON
data_books = read_json('data/books.json')

# liste_auteurs(data_books)